Fully furnished short-term rental apartments in Cincinnati, Oh. Including all utilities, fast internet/wifi, new modern furniture, BIG TVs with expanded cable, washer & dryer in your unit, parking, convenient location close to parks, shopping, dining and entertainment.

Address: 7631 Montgomery Rd, Cincinnati, OH 45236

Phone: 513-706-8127
